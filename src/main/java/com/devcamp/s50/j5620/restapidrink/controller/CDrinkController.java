package com.devcamp.s50.j5620.restapidrink.controller;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.j5620.restapidrink.restapi.model.CDrink;

@RestController
public class CDrinkController {
    @CrossOrigin
    @GetMapping("/devcamp-drinks")
    public ArrayList<CDrink> getDrink(){
        // khai báo danh sách
        ArrayList<CDrink> listdink = new ArrayList<CDrink>();
        LocalDate today = LocalDate.now(ZoneId.systemDefault());
        // khởi tạo đối tượng
        CDrink tratac = new CDrink(1, "TRATAC", "Tra Tac", 10000, today, today);
        CDrink coca = new CDrink(1, "COCA", "Cocacola", 15000, today, today);
        CDrink pepsi = new CDrink(1, "PEPSI", "Pepsi", 15000, today, today);
        CDrink monter = new CDrink(1, "MONTER", "Monter", 20000, today, today);

        listdink.add(tratac);
        listdink.add(coca);
        listdink.add(pepsi);
        listdink.add(monter);
        return listdink;
    }

}
