package com.devcamp.s50.j5620.restapidrink;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApiDrinkApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestApiDrinkApplication.class, args);
	}

}
