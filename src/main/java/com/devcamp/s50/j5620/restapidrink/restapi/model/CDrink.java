package com.devcamp.s50.j5620.restapidrink.restapi.model;

import java.time.LocalDate;

public class CDrink {
    private int id;
    private String maNuocUong;
    private String tenNuocUong;
    private long price;
    private LocalDate ngayTao;
    private LocalDate ngayCaoNhap;

    public CDrink(){
        super();
    }

    public CDrink(int id,String maNuocUong,String tenNuocUong,long price,LocalDate ngayTao,LocalDate ngayCaoNhap){
        this.id = id ;
        this.maNuocUong = maNuocUong;
        this.tenNuocUong = tenNuocUong;
        this.price = price;
        this.ngayTao = ngayTao;
        this.ngayCaoNhap = ngayCaoNhap;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMaNuocUong() {
        return maNuocUong;
    }

    public void setMaNuocUong(String maNuocUong) {
        this.maNuocUong = maNuocUong;
    }

    public String getTenNuocUong() {
        return tenNuocUong;
    }

    public void setTenNuocUong(String tenNuocUong) {
        this.tenNuocUong = tenNuocUong;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public LocalDate getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
    }

    public LocalDate getNgayCaoNhap() {
        return ngayCaoNhap;
    }

    public void setNgayCaoNhap(LocalDate ngayCaoNhap) {
        this.ngayCaoNhap = ngayCaoNhap;
    }

    @Override
    public String toString() {
        return "CDrink [id=" + id + ", maNuocUong=" + maNuocUong + ", ngayCaoNhap=" + ngayCaoNhap + ", ngayTao="
                + ngayTao + ", price=" + price + ", tenNuocUong=" + tenNuocUong + "]";
    }
  
}
